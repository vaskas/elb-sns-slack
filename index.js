var Slack = require('node-slackr')
 , config = require('./config.json')

var slack = new Slack(config.webhookUrl, { channel: config.channel })

exports.handler = function(event, context) {
    var m = JSON.parse(event.Records[0]["Sns"]["Message"])
      , n = m.Trigger.Namespace + ' ' + m.Trigger.MetricName
      , d = m.Trigger.Dimensions[0]

    var msg  = '*' + n + ' (' + d.value + ')* ' + m.AlarmName + ' '
        msg += m.NewStateValue + ': ' + m.NewStateReason
        msg += ', see https://console.aws.amazon.com/cloudwatch/home?region='
        msg += config.region + '#s=Alarms&alarm=' + m.AlarmName

    slack.notify(msg, function(err, result) {
        if(err) console.log(err,result)
    })
}
