# SNS ELB notifications to Slack relay

This is a little AWS lambda script based on the http://www.digitaljedi.ca/2015/05/06/lambda-sns-cloudwatch-alarms-slack-integration/ post.

To install,

```
cp config.json.sample config.json
vi config.json
make
```

Then upload it to AWS lambda and configure with your events.
